# JavaScript - Komputer Store

Project was built using JavaScript, HTML5 and css.

This is a web page called komputer store, where you can buy computers. But first, you will need the money to buy a laptop from the list.
You can "work" for money, and bank it, or you can take out a loan by up to double of your current bank value.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Running the project

Simply open the index.html file in a browser to try it out!

## Maintainers 

[Robin Burø (@Robbur)](https://gitlab.com/Robbur)

## License

---
Copyright 2020, Robin Burø ([@Robbur](https://gitlab.com/Robbur))