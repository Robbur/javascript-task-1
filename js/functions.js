// HTML Variables
const bankBalanceDOM = document.getElementById("bankBalance");
const workBalanceDOM = document.getElementById("workBalance");
const laptopFeaturesDOM = document.getElementById("laptopFeatures");
const laptopTitleDOM = document.getElementById("laptopTitle");
const laptopInformationDOM = document.getElementById("laptopInformation");
const laptopPriceDOM = document.getElementById("laptopPrice");
const laptopImageDOM = document.getElementById("laptopImage");

// HTML Buttons
const loanButtonDOM = document.getElementById("loanButton");
const bankButtonDOM = document.getElementById("bankButton");
const workButtonDOM = document.getElementById("workButton");
const buyButtonDOM = document.getElementById("buyButton");
const laptopDropdownDOM = document.getElementById("laptopsDropdown");

// JS Variables
let receivedLoan = false;
let bankBalance = 700;
let workBalance = 0;
let currentLaptopPrice = 0;
let laptopsArray = [{ name: "Lenovo 400GYZ", price: 1400, description: "This is a great computer for a low price. Decent CPU for normal use", features: "Decent CPU\nDecent screen\nDecent battery life", image: "img/Laptop1.png" },
{ name: "HP G16 1500", price: 5000, description: "This is a powerful computer that can handle any task. It is quite expensive, but worth the money!", features: "Great CPU\nGreat screen\nGreat battery life", image: "img/Laptop2.png" },
{ name: "Acer Gaming Station", price: 3299, description: "Decent gaming computer fo your average gamer. The computer will not max out the settings, but will give you a nice gaming experience", features: "Great CPU\nGreat screen\nOk battery life", image: "img/Laptop3.png" },
{ name: "Dell Work Laptop", price: 1399, description: "This is a decent work laptop that can handle most tasks for a good price", features: "Ok CPU\nOk screen\nOk battery life", image: "img/Laptop4.png" }
];

// Renders the DOM
const renderDOM = () => {
    bankBalanceDOM.innerText = "Balance: " + bankBalance + " Kr.";
    workBalanceDOM.innerText = "Pay: " + workBalance + " Kr.";
    let name = laptopDropdownDOM.options[laptopDropdownDOM.selectedIndex].value;
    for (let i = 0; i < laptopsArray.length; i++) {
        if (name == laptopsArray[i].name) {
            laptopFeaturesDOM.innerText = "Features: \n" + laptopsArray[i].features;
            laptopTitleDOM.innerText = laptopsArray[i].name;
            laptopInformationDOM.innerText = laptopsArray[i].description;
            laptopPriceDOM.innerText = "Price: " + laptopsArray[i].price + " Kr.";
            laptopImageDOM.src = laptopsArray[i].image;
            currentLaptopPrice = laptopsArray[i].price;
        }
    }
}

// Get a loan
const loanAction = () => {
    if (receivedLoan == false) {
        let loanAmount = prompt("Enter amount: ");
        if (loanAmount <= (bankBalance * 2) && receivedLoan == false) {
            receivedLoan = true;
            bankBalance += parseInt(loanAmount);
            renderDOM()
        }
        else {
            alert("You don't have enough money to loan that much!");
            return;
        }
    }
    else {
        alert("Sorry, you already have a loan!");
        return;
    }

}

// Work for money!
const workAction = () => {
    workBalance += 100;
    renderDOM()
}

// Secure the money!
const bankAction = () => {
    if (workBalance > 0) {
        bankBalance += workBalance;
        workBalance = 0;
        renderDOM()
    } else {
        alert("You have no money to put in the bank, do some work!");
    }
}

// Buy a laptop
const buyAction = () => {
    if (bankBalance >= currentLaptopPrice) {
        bankBalance -= currentLaptopPrice;
        renderDOM()
        alert("Congratulations, you are now the owner of a new laptop!");
    } else {
        alert("You do not have enough money, keep working!");
    }
}

// Custom alert box


// EventListeners
workButtonDOM.addEventListener('click', workAction);
bankButtonDOM.addEventListener('click', bankAction);
loanButtonDOM.addEventListener('click', loanAction);
buyButtonDOM.addEventListener('click', buyAction);
laptopDropdownDOM.addEventListener('change', renderDOM);

// Fills out the dropdown box
for (let i = 0; i < laptopsArray.length; i++) {
    let opt = laptopsArray[i].name;

    let el = document.createElement("option");
    el.setAttribute('value', laptopsArray[i].name);
    el.text = opt;

    laptopDropdownDOM.appendChild(el);
}

renderDOM()

